# Text Prediction RNN Project

This is an individual project done to showcase an introductory example of Recurrent Neural Networks (RNNs).

This project uses Python 3.9.
You can find the program code (Text Prediction RNN Project.py).

This is a very very very basic python code that predicts the next word while typing, form previously typed text.